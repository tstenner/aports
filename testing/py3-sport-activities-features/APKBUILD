# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-sport-activities-features
pkgver=0.2.16
pkgrel=0
pkgdesc="A minimalistic toolbox for extracting features from sport activity files"
url="https://github.com/firefly-cpp/sport-activities-features"
arch="noarch !s390x !riscv64" # py3-niaaml
license="MIT"
depends="
	python3
	py3-dotmap
	py3-geopy
	py3-geotiler
	py3-gpxpy
	py3-matplotlib
	py3-niaaml
	py3-overpy
	py3-requests
	py3-tcxreader
	"
checkdepends="python3-dev py3-pytest"
makedepends="py3-build py3-poetry-core py3-wheel py3-installer"
source="https://github.com/firefly-cpp/sport-activities-features/archive/$pkgver/sport-activities-features-$pkgver.tar.gz"
builddir="$srcdir/sport-activities-features-$pkgver"

build() {
	GIT_DIR="$builddir" python3 -m build --no-isolation --wheel
}

check() {
	python3 -m pytest -k "not test_weather"
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/sport_activities_features-$pkgver-py3-none-any.whl
}

sha512sums="
64c4b69b976804cf4d6ae5e432e4c24b2d1dce9e5571da6cb56a609ac5dde3d9639b019a13b58c43171e79810ed0e218a5ff7ac49a8f8fda14c62bd39a57e8fc  sport-activities-features-0.2.16.tar.gz
"
