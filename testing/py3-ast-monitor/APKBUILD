# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-ast-monitor
pkgver=0.1.4
pkgrel=1
pkgdesc="A wearable Raspberry Pi computer for cyclists"
url="https://github.com/firefly-cpp/AST-Monitor"
arch="noarch !s390x !riscv64 !armhf !ppc64le" # py3-sport-activities-features # py3-pyqt-feedback-flow
license="MIT"
depends="
	python3
	py3-geopy
	py3-matplotlib
	py3-pyqt-feedback-flow
	py3-qt5
	py3-qtwebengine
	py3-sport-activities-features
	py3-tcxreader
	"
checkdepends="python3-dev py3-pytest"
makedepends="py3-build py3-poetry-core py3-wheel py3-installer"
source="https://github.com/firefly-cpp/AST-Monitor/archive/$pkgver/ast_monitor-$pkgver.tar.gz"
builddir="$srcdir/AST-Monitor-$pkgver"

build() {
	GIT_DIR="$builddir" python3 -m build --no-isolation --wheel
}

check() {
	python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/ast_monitor-$pkgver-py3-none-any.whl
}

sha512sums="
30636d3fb45baa83f605dcf35710a6ff8fe48678832c043adc982aa2f4e822e9a814cb00d3126585008515c5490a74ced8ffbea25f00c3480075537cbe847db2  ast_monitor-0.1.4.tar.gz
"
