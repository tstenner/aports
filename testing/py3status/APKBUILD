# Contributor: Guy Godfroy <guy.godfroy@gugod.fr>
# Maintainer: Guy Godfroy <guy.godfroy@gugod.fr>
pkgname=py3status
pkgver=3.44
pkgrel=0
pkgdesc="Extensible i3status wrapper written in python"
url="https://py3status.readthedocs.io"
arch="noarch"
license="BSD-3-Clause"
depends="python3 py3-setuptools"
makedepends="py3-setuptools"
checkdepends="py3-pytest"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/ultrabug/py3status/archive/refs/tags/$pkgver.tar.gz"

build() {
	python3 setup.py build
}

check() {
	pytest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
	install -d "$pkgdir/usr/share/doc/$pkgname"
	install -d "$pkgdir/usr/share/doc/$pkgname/dev-guide"
	install -d "$pkgdir/usr/share/doc/$pkgname/user-guide"
	install -m644 docs/*.md README.md CHANGELOG "$pkgdir/usr/share/doc/$pkgname"
	install -m644 docs/dev-guide/* "$pkgdir/usr/share/doc/$pkgname/dev-guide"
	install -m644 docs/user-guide/* "$pkgdir/usr/share/doc/$pkgname/user-guide"
	install -Dm644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

sha512sums="
f135c5793759350ec4c58af8b80e56e3c25359675da7ccd8e6c1da994f67df13259a3d4f2b9d9047b9fcb0bf14f08526630a60f0f148bf364ce874fc13fb162a  py3status-3.44.tar.gz
"
