# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qt6-qtshadertools
pkgver=6.3.0
pkgrel=1
pkgdesc="Experimental module providing APIs and a host tool to host tool to perform graphics and compute shader conditioning"
url="https://qt.io/"
arch="all"
license="LGPL-2.1-only AND LGPL-3.0-only AND GPL-3.0-only AND Qt-GPL-exception-1.0"
depends_dev="
	qt6-qtbase-dev
	vulkan-headers
	"
makedepends="$depends_dev
	cmake
	perl
	samurai
	"
subpackages="$pkgname-dev"
options="!check" # No tests
builddir="$srcdir/qtshadertools-everywhere-src-${pkgver/_/-}"

case $pkgver in
	*_alpha*|*_beta*|*_rc*) _rel=development_releases;;
	*) _rel=official_releases;;
esac

source="https://download.qt.io/$_rel/qt/${pkgver%.*}/${pkgver/_/-}/submodules/qtshadertools-everywhere-src-${pkgver/_/-}.tar.xz
	0001-Fix-encoding-decoding-of-string-literals-for-big-end.patch
	"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e104fa3b04692c3c79c1caae738642e92fe077761a5d5b064ed201b8856b409677824d84cbc954bfb21e41aaa25b0d8808692c78cf787432b85604af6706e2bd  qtshadertools-everywhere-src-6.3.0.tar.xz
62773de34ba83ca791a47e4bd06bc0f08a5309cd9e2f710226f2a152595be8fe2952208c5db576cc56fff9dab3e0927f90afb1b440ff1bc6ac73180bc96a5d07  0001-Fix-encoding-decoding-of-string-literals-for-big-end.patch
"
