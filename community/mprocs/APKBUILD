# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=mprocs
pkgver=0.4.0
pkgrel=0
pkgdesc="Run multiple commands in parallel"
url="https://github.com/pvolok/mprocs"
arch="aarch64 armhf armv7 ppc64le x86 x86_64"  # blocked by rust/cargo
license="MIT"
makedepends="cargo"
source="https://github.com/pvolok/mprocs/archive/v$pkgver/mprocs-$pkgver.tar.gz"

export CARGO_PROFILE_RELEASE_CODEGEN_UNITS=1
export CARGO_PROFILE_RELEASE_LTO="true"
export CARGO_PROFILE_RELEASE_OPT_LEVEL="z"
export CARGO_PROFILE_RELEASE_PANIC="abort"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	cargo build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -D -m755 target/release/mprocs -t "$pkgdir"/usr/bin/
}

sha512sums="
6941aada984fdd8941f57ce7337cc7f9a6b891005c9820ae4cdb16c5a1dfca4c18b9a2e6f2ebc812f99f49a40a8f9631358add02a136c28fbf378eb2a2d6e490  mprocs-0.4.0.tar.gz
"
