# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=gst-libav
pkgver=1.20.2
pkgrel=0
pkgdesc="GStreamer streaming media framework libav plugin"
url="https://gstreamer.freedesktop.org"
arch="all"
license="GPL-2.0-or-later LGPL-2.0-or-later"
makedepends="coreutils ffmpeg-dev gst-plugins-base-dev gstreamer-dev meson
	orc-dev"
source="https://gstreamer.freedesktop.org/src/gst-libav/gst-libav-$pkgver.tar.xz"
replaces="gst-libav1"

build() {
	abuild-meson \
		-Dpackage-origin="https://alpinelinux.org" \
		-Dpackage-name="GStreamer libav plugin (Alpine Linux)" \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test -v --no-rebuild -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

doc() {
	default_doc
	replaces="${pkgname}1-doc"
}

sha512sums="
845acd0144f333fc532c35a854a9773cef70c96b097995a684911b9ac3fe45aaf75011454e736427dbd6afabaf07459fd22cf7ce8543066e1d5547ee1992e2ca  gst-libav-1.20.2.tar.xz
"
