# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwallet-pam
pkgver=5.25.0
pkgrel=0
pkgdesc="KWallet PAM integration"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kwallet
arch="all !armhf !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-or-later"
depends="socat"
makedepends="
	extra-cmake-modules
	kwallet-dev
	libgcrypt-dev
	linux-pam-dev
	samurai
	socat
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kwallet-pam-$pkgver.tar.xz"
options="!check" # No tests available

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# We don't ship systemd
	rm -r "$pkgdir"/usr/lib/systemd
}

sha512sums="
9d4690a78cc6d17ba2baf56164c0761d6b6038d3c19628e8104503df36d6d513ffc26b79c1771d6749d16d934b7d0bf31ab9d54c2ac41d41e6d279048cbccbef  kwallet-pam-5.25.0.tar.xz
"
