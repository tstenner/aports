# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-vault
pkgver=5.25.0
pkgrel=0
pkgdesc="Plasma applet and services for creating encrypted vaults"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="(GPL-2.0-only OR GPL-3.0-only) AND (LGPL-2.1-only AND LGPL-3.0-only)"
makedepends="
	extra-cmake-modules
	kactivities-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	libksysguard-dev
	networkmanager-qt-dev
	plasma-framework-dev
	qt5-qtbase-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-vault-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3e068fdd0c66693de5f79380fcbea498264e7533fa121c0f4ed938fba0b1d6b6333b7b76f8456887ffa6f2f056cb5b57fb4b5d85f2fdc0f7aa5e4147ccfa560e  plasma-vault-5.25.0.tar.xz
"
