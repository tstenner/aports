# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdeplasma-addons
pkgver=5.25.0
pkgrel=0
pkgdesc="All kind of addons to improve your Plasma experience"
# armhf blocked by qt5-qtdeclarative
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.0-only AND GPL-2.0-or-later"
depends="purpose"
depends_dev="
	karchive-dev
	kcmutils-dev
	kconfig-dev
	kcoreaddons-dev
	kdeclarative-dev
	kholidays-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knewstuff-dev
	knotifications-dev
	kross-dev
	krunner-dev
	kservice-dev
	kunitconversion-dev
	kwindowsystem-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtwebengine-dev
	samurai
	sonnet-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kdeplasma-addons-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# converterrunnertest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "converterrunnertest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9105e9862f2dd890d39c3ad5bb9e62e7b9907e67359ef65e97d98675cd816487e4e455ee3faf43b0de2269faab917f921795ec143fa22f96ccfa97540a133857  kdeplasma-addons-5.25.0.tar.xz
"
