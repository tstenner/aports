# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-sdk
pkgver=5.25.0
pkgrel=0
pkgdesc="Applications useful for Plasma Development"
# armhf blocked by qt5-qtdeclarative
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="
	kirigami2
	qt5-qtquickcontrols
	"
makedepends="
	extra-cmake-modules
	karchive-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kparts-dev
	kservice-dev
	ktexteditor-dev
	kwidgetsaddons-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtsvg-dev
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-sdk-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# iconmodeltest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "iconmodeltest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0d8d2eb65dc8ea12623a32b859620d8abbb3498ac9b843d4e645c0721a2ceb9918d284e7aa8d3f1f02291004dca34b531c1b7003ad3155914d8ff6760234a70c  plasma-sdk-5.25.0.tar.xz
"
